# userdbadm

## Easy management of pam_userdb databases

userdbadm is a Perl script for managing a userdb database with user names and crypted passwords, as
used e. g. by PAM's `pam_userdb` module. You will need such a database for example if you want to
create virtual users for [vsftpd](https://security.appspot.com/vsftpd.html).

## Homepage

The project's official homepage with further information is
<https://nasauber.de/opensource/userdbadm/>.
